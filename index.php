<?php
function assets($path = '/') { return '/assets/' . ltrim($path, '/'); }
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<title>New Found Joy</title>
<meta name='robots' content='noindex,follow' />
<link rel='stylesheet' href='<?php echo assets('/css/style.css'); ?>' type='text/css' media='all' />
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="holding">

<header>
  <div id="top-banner">
    <div class="container">
    <div class="col-xs-12 text-center">
      <img src="<?php echo assets('/img/nfj_logo.png'); ?>" id="nfj-logo"/>
      </div>
      <div class="col=xs=12 text-center">
      <div class="hero-unit title text-center"><h1 id="tagline">The home of yummy, Gluten free treats!</h1></div></div>

      <!--<div id="get-started"><a class="btn btn-yellow btn-script btn-large" href="#">Find Out More</a></div>-->
    </div>
  </div>
</header>

<!-- <nav id="primarynav">
  <div class="container">
    <div class="row">
      <div class="col-sm-9">
        <div class="menu-primary-navigation-container">
          <ul id="menu-primary-navigation" class="navbar menu">
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="#">Our Food</a>
            </li>
            <li>
              <a href="#">Contact</a>
            </li>
            <li>
              <a href="#">How it Works</a>
            </li>
          </ul>
        </div>
      </div>
     <div class="right-badge">
        <img src="<?php echo assets('/img/gluten-free.png'); ?>" />
      </div>
    </div>
  </div>
</nav>   -->


<section id="how-it-works">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 step">
        <div class="icon">
          <i class="flaticon-cupcake3"></i>
        </div>
        <div class="title">Delicious, tempting gluten free treats</div>
        <div class="description">Homemade in a separate Gluten free bakery, individually wrapped for safety and convenience.</div>
      </div>
      <div class="col-sm-4 step">
        <div class="icon">
          <i class="flaticon-shopping93"></i>
        </div>
        <div class="title">Mixed box including</div>
        <div class="description">9x Caramel Square<br/> 9x Diana Delight<br/>9x Almond Square<br/>9x Chocolate Brownie<br/>9x Rocky Road<br/>Suitable for freezing up to 6 months and 10 days shelf life on defrost </div>
      </div>
       <div class="col-sm-4 step">
        <div class="icon">
          <i class="flaticon-placeholder8"></i>
        </div>
        <div class="title">Try it today</div>
        <div class="description">Our range of exciting, gluten free treats are designed to leave you wanting more. Try it today, New Found Joy - The home of yummy, Gluten free treats!</div>
      </div>
    </div>
  </div>
</section>
<section class="contact">
<div class="container text-center">
<div class="title">
Contact Us
</div>
<div class="row">
<div class="col-sm-4">
</div>
<div class="col-sm-4">
<address>
Telephone: 02838316012<br/>
Email: carol@newfoundjoy.co.uk<br/>

30 Queens Place Lurgan, Co Armagh,<br/> BT668BY
</address>
</div>
<div class="col-sm-4">
</div>


</div>
</div>
</section>


<!-- <section class="products">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <div class="product" style="background-image:url(<?php echo assets('/img/samples/product1.jpg'); ?>);">
          <img src="<?php echo assets('/img/product-overlay.png'); ?>" class="frame">
          <div class="name">
            <div class="valign-wrapper valign-fullheight">
              <div class="valign-object">Product 1</div>
            </div>
          </div>
          <div class="more">
            <div class="valign-wrapper valign-fullheight">
              <div class="valign-object"><a href="#" class="btn btn-yellow btn-chunky">Eat Me</a></div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="product" style="background-image:url(<?php echo assets('/img/samples/product2.jpg'); ?>);">
          <img src="<?php echo assets('/img/product-overlay.png'); ?>" class="frame">
          <div class="name">
            <div class="valign-wrapper valign-fullheight">
              <div class="valign-object">Product 2</div>
            </div>
          </div>
          <div class="more">
            <div class="valign-wrapper valign-fullheight">
              <div class="valign-object"><a href="#" class="btn btn-yellow btn-chunky">Eat Me</a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> -->

<footer>
  <div class="container">
    <div class="social">
      <a class="facebook" href="#facebook"><i class="flaticon-facebook31"></i></a>
      <a class="twitter" href="#twitter"><i class="flaticon-twitter29"></i></a>
    </div>
  </div>
</footer>
</body>
</html>